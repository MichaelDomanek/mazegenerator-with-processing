public enum Direction {
    //North(new Position(-1, 0)), East(new Position(0, 1)), South(new Position(-1, 0)), West(new Position(0, -1));
    North(0, 1), East(1, 0), South(0, -1), West(-1, 0);

    //private Position position;
    private int x;
    private int y;

    private Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
}

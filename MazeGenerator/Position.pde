public class Position {
    public int x;
    public int y;

    Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public Position getMoveToTile(Position pos) {        
        if(this == pos){
            return new Position(0, 0);
        }
        ArrayList<Direction> temp = new ArrayList();
        if(this.x < pos.x){
            temp.add(Direction.East);
        }
        else if(this.x > pos.x){
            temp.add(Direction.West);
        }
        if(this.y < pos.y){
            temp.add(Direction.North);
        }
        else if(this.y > pos.y){
            temp.add(Direction.South);
        }
        if(temp.size() > 0){
            int rand = ThreadLocalRandom.current().nextInt(0, temp.size());
            Direction dir = temp.get(rand);
            Position p = new Position(dir.x, dir.y);
            return p;
        }
        return new Position(0, 0);
    }
    
    public Position[] getMovesToTile(Position pos) {
        if(this == pos){
            return new Position[0];
        }
        ArrayList<Direction> temp = new ArrayList();
        if(this.x > pos.x){
            temp.add(Direction.East);
        }
        else if(this.x < pos.x){
            temp.add(Direction.West);
        }
        if(this.y > pos.y){
            temp.add(Direction.North);
        }
        else if(this.x < pos.x){
            temp.add(Direction.South);
        }
        if(temp.size() > 0){
            Position[] tempArray = new Position[temp.size()];
            for (int i = 0; i < temp.size(); i++) {
                tempArray[i] = new Position(temp.get(i).x, temp.get(i).y);
            }
        }
        return new Position[0];
    }
    
    
    public Position[] getAllMoves(int[][] maze) {
        ArrayList<Direction> temp = new ArrayList();
        if(x < maze.length - 2){
            if(maze[x + 2][y] != 0){
                temp.add(Direction.East);
            }
        }
        if(x - 2 >= 0){
            if(maze[x - 2][y] != 0){
                temp.add(Direction.West);
            }
        }
        if(y < maze[0].length - 2){
            if(maze[x][y + 2] != 0){
                temp.add(Direction.North);
            }
        }
        if(y - 2 >= 0){
            if(maze[x][y - 2] != 0){
                temp.add(Direction.South);
            }
        }
        if(temp.size() > 0){
            Position[] tempArray = new Position[temp.size()];
            for (int i = 0; i < temp.size(); i++) {
                tempArray[i] = new Position(temp.get(i).x, temp.get(i).y);
            }
            return tempArray;
        }
        return new Position[0];
    }
    
    public String toString(){
        String s = "(" + x + ", " + y + ")";
        return s;
    }   

    
    public Position add(Position pos){
        Position p = new Position(this.x + pos.x, this.y + pos.y);
        return p;
    }
}

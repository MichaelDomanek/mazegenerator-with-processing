/*
Maze by Michael Domanek
*/

Generator mg;

void setup(){
    //size(1180, 820);
    fullScreen();
    background(5);
    long startTime = System.nanoTime();
    mg = new Generator(5); //tilesize starts at 2
    mg.generate();
    mg.show();
    mg.getMaze();
    //mg.saveToFile("maze.txt");
    long endTime = System.nanoTime();
    println((endTime - startTime) / 1000000, "milliseconds for Generation");
    noLoop();
    //saveFrame(".\\Pictures\\Maze.png");
}

import java.util.concurrent.ThreadLocalRandom;

class Generator {

    private int[][] maze;
    private int size;
    private int mazeWidth;
    private int mazeHeight;
    private Position currentPos;
    private Position goal;

    public Generator(int size) {
        this.size = size;
        mazeWidth = width / size;
        mazeHeight = height / size;
        maze = new int[mazeWidth][];
        for (int i=0; i < mazeWidth; i++) {
            maze[i] = new int[mazeHeight];
            for (int j=0; j < mazeHeight; j++) {
                maze[i][j] = 1;
            }
        }
        currentPos = new Position(0, 0);
        goal = new Position(mazeWidth - 2, mazeHeight - 2);
        maze[goal.x][goal.y] = 2;
    }

    public void generate() {
        ArrayList<Position> directions = new ArrayList();
        Position[] surroundings;
        Position nextPos;
        surroundings = new Position[0];
        nextPos = new Position(0, 0);
        directions.add(nextPos);
        int counter = 0;
        do{
            do{
                if(counter++ % 2 == 0){
                    maze[currentPos.x][currentPos.y] = 0;
                    surroundings = currentPos.getAllMoves(maze);
                    if(surroundings.length > 0){
                        if(surroundings.length > 1){
                            directions.add(currentPos);
                        }
                        nextPos = surroundings[ThreadLocalRandom.current().nextInt(0, surroundings.length)];
                        currentPos = currentPos.add(nextPos);
                    }
                }
                else{
                    maze[currentPos.x][currentPos.y] = 0;
                    currentPos = currentPos.add(nextPos);
                }
            }while(surroundings.length > 0);
            directions.remove(currentPos);
            if(directions.size() > 0){
                currentPos = directions.get(0);
            }
        }while(directions.size() > 0);
        if(mazeWidth % size == 0){
        }
        goal = new Position(mazeWidth - 2, mazeHeight - 2);
        maze[goal.x][goal.y] = 2;
    }

    public void generateRandom() {
        int random = 0;
        int random2 = 0;
        for (int i=0; i < 2000; i += 10) {
            random = ThreadLocalRandom.current().nextInt(0, mazeWidth);
            random2 = ThreadLocalRandom.current().nextInt(0, mazeHeight);
            maze[random][random2] = 0;
        }
    }

    public int[][] getMaze(){
        int[][] temp = new int[maze[0].length][];
        for(int i=0; i < temp.length; i++){
            temp[i] = new int[maze.length];
        }
        
        for(int i=0; i < maze.length; i++){
            for(int j=0; i < maze[0].length; i++){
                temp[j][i] = maze[i][j];
            }
        }
        return temp;
    }
    
    public void printText() {
        for (int i=0; i < mazeWidth; i++) {
            for (int j=0; j < mazeHeight; j++) {
                print(maze[j][i] + " ");
            }    
            print("\n");
        }
    }

    public void show() {
        for (int i=0; i < mazeWidth; i++) {
            for (int j=0; j < mazeHeight; j++) {
                if (maze[i][j] == 0) {
                    fill(0, 205, 255);
                    stroke(0, 205, 255);
                    //fill(255);
                    //stroke(255);
                    rect(i * size, j * size, size, size);
                }
            }
        }
        fill(255, 20, 20);
        stroke(255, 20, 20);
        rect(goal.x * size, goal.y * size, size, size);
    }

    public void saveToFile(String filename){
        String mazeString[];
        String s;
        mazeString = new String[mazeHeight];
        for (int i=0; i < mazeHeight; i++) {
            if(i == 0){
            s = "{{";
            }
            else{
                s = "{";
            }
            for (int j=0; j < mazeWidth; j++) {
                s += str(maze[j][i]);
                if(j < mazeWidth - 1){
                    s += ", ";
                }
                else{
                    if(i < mazeHeight - 1 || j < mazeWidth - 1){
                        s += "},";
                    }
                    else{
                        s += "}}";
                    }
                }
            }
            mazeString[i] = s;
        }
        saveStrings(filename, mazeString);
    }
}
